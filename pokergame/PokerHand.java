/*This project was written for the project of the subject "Programming and 
 * Software Development" (COMP90041).The name of this project is "The Game 
 * of Poker", which can realize simple five-card game.*/

/*PokerHand class includes judging the input cards valid or not and output the 
 *description of the category of the input five cards. This class also can 
 *provide the method to sort the value of one hand card according to the order 
 *of importance.*/
public class PokerHand{
	private int cardvalue1,cardvalue2,cardvalue3,cardvalue4,cardvalue5;
	private char cardname1,cardname2,cardname3,cardname4,cardname5;
	private int[] cardvalue = new int[5];
	private int[] rank = new int[5];
	private boolean r1, r2, r3, r4;
	private boolean sameCardType, straightIf;
	private static final String[] output = 
		{"2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"};
	private int cardLevel;

	/*The method valueName uses two private methods cardNumber and cardType to 
	 * change two-character strings to two parts: value and type.*/
	public void valueName
	    (String card1,String card2,String card3,String card4,String card5){
		cardvalue1 = cardNumber(card1);
        cardvalue2 = cardNumber(card2);
        cardvalue3 = cardNumber(card3);
        cardvalue4 = cardNumber(card4);
        cardvalue5 = cardNumber(card5);
        
        cardname1 = cardType(card1);
        cardname2 = cardType(card2);
        cardname3 = cardType(card3);
        cardname4 = cardType(card4);
        cardname5 = cardType(card5);
	}
	
	/*Sort the cardvalue array in ascending order.*/
	public void sortArray(){
		cardvalue[0]=cardvalue1;
		cardvalue[1]=cardvalue2;
		cardvalue[2]=cardvalue3;
		cardvalue[3]=cardvalue4;
		cardvalue[4]=cardvalue5;
		selectionsort(cardvalue);
	}
	
	public int[] getArray(){
	    return this.cardvalue;
    }
	
	
	public int getCardLevel(){
	    return this.cardLevel;
    }
	
	public int[] getRank(){
	    return this.rank;
    }
	
	/*The pokerClass method is used to determine poker hand.*/
	public void pokerClass(){
		fourOfAKind(cardvalue);
		fullHouse(cardvalue);
		threeOfAKind(cardvalue);
		twoPair(cardvalue);
		onePair(cardvalue);
		straightFlush(cardvalue);
		flush(cardvalue);
		straight(cardvalue);
		high(cardvalue);
	}
	
	
	/*The relation method is used to compare if the adjacent two card equal or
	 * not.*/
	private void relation(){
		r1=(cardvalue[0]==cardvalue[1]);
		r2=(cardvalue[1]==cardvalue[2]);
		r3=(cardvalue[2]==cardvalue[3]);
		r4=(cardvalue[3]==cardvalue[4]);
	}
	
	/* If there is at least one pair, the poker hand of cards should be one of
	 *  four of a kind, full house, three of a kind, two pair or one pair. In 
	 *  this situation, we do not need to consider card name. According to the
	 *  relation between adjacent two cards, we can find how many cards are 
	 *  equal in one hand and determine the type of poker hand.*/
	private void fourOfAKind(int[] a){
		relation();
		if((!r1)&&r2&&r3&&r4){
			cardLevel=8;
		    System.out.println("Four "+output[a[2]-2]+"s");	
		    }
		if(r1&&r2&&r3&&(!r4)){
			cardLevel=8;
		    System.out.println("Four "+output[a[2]-2]+"s");
		    }
	}
	
	private void fullHouse(int[] a){
		relation();
		if(r1&&r2&&(!r3)&&r4){
			cardLevel=7;
		    System.out.println(output[a[1]-2]+"s full of "+output[a[4]-2]+"s");
		    }
		if(r1&&(!r2)&&r3&&r4){
			cardLevel=7;
		    System.out.println(output[a[4]-2]+"s full of "+output[a[1]-2]+"s");
		    }
	}
	
	private void threeOfAKind(int[] a){
		relation();
		if(r1&&r2&&(!r3)&&(!r4)){
			cardLevel=4;
		    System.out.println("Three "+output[a[1]-2]+"s");
		    }
		if((!r1)&&(!r2)&&r3&&r4){
			cardLevel=4;
		    System.out.println("Three "+output[a[3]-2]+"s");
		    }
		if((!r1)&&r2&&r3&&(!r4)){
			cardLevel=4;
			System.out.println("Three "+output[a[3]-2]+"s");
			}
	}
	
	private void twoPair(int[] a){
		relation();
		if((!r1)&&r2&&(!r3)&&r4){
			cardLevel=3;
		    System.out.println(output[a[4]-2]+"s over "+output[a[1]-2]+"s");	
		    }
		if(r1&&(!r2)&&(!r3)&&r4){
			cardLevel=3;
		    System.out.println(output[a[4]-2]+"s over "+output[a[1]-2]+"s");	
		    }
		if(r1&&(!r2)&&r3&&(!r4)){
			cardLevel=3;
			System.out.println(output[a[3]-2]+"s over "+output[a[1]-2]+"s");	
			}
	}
	
	private void onePair(int[] a){
		relation();
		if(r1&&(!r2)&&(!r3)&&(!r4)){
			cardLevel=2;
		    System.out.println("Pair of "+output[a[0]-2]+"s");
		    }
		if((!r1)&&r2&&(!r3)&&(!r4)){
			cardLevel=2;
		    System.out.println("Pair of "+output[a[1]-2]+"s");
		    }
		if((!r1)&&(!r2)&&r3&&(!r4)){
			cardLevel=2;
			System.out.println("Pair of "+output[a[2]-2]+"s");	
			}
		if((!r1)&&(!r2)&&(!r3)&&r4){
			cardLevel=2;
			System.out.println("Pair of "+output[a[3]-2]+"s");
			}
	}
	
	/*Determine if the poker hand has the same card type or not.*/
	private void sameCardName(){
		sameCardType=(cardname1==cardname2)&&(cardname1==cardname3)&&
		        (cardname1==cardname4)&&(cardname1==cardname5);
	}
	
	/*Determine if the card value in the poker hand increase by 1 in turn.*/
	private void straightOrNot(int[] a){
		straightIf=((a[4]-a[3])==1)&&((a[3]-a[2])==1)&&((a[2]-a[1])==1)&&
				((a[1]-a[0])==1);
	}
	
	/*If there is no pair in one hand, the poker hand of cards should be one of
	 * straight flush, flush, straight or high.*/
	private void straightFlush(int[] a){
		sameCardName();
		straightOrNot(cardvalue);
		relation();
		if((!r1)&&(!r2)&&(!r3)&&(!r4)&&sameCardType&&straightIf){
			cardLevel=9;
		    System.out.println(output[a[4]-2]+"-high straight flush");	
		    }

	}
	
	private void flush(int[] a){
		sameCardName();
		straightOrNot(cardvalue);
		relation();
		if((!r1)&&(!r2)&&(!r3)&&(!r4)&&sameCardType&&(!straightIf)){
			cardLevel=6;
		    System.out.println(output[a[4]-2]+"-high flush");
		    }
	}
	
	private void straight(int[] a){
		sameCardName();
		straightOrNot(cardvalue);
		relation();
		if((!r1)&&(!r2)&&(!r3)&&(!r4)&&(!sameCardType)&&straightIf){
			cardLevel=5;
		    System.out.println(output[a[4]-2]+"-high straight");
		    }
	}
	
	private void high(int[] a){
		sameCardName();
		straightOrNot(cardvalue);
		relation();
		if((!r1)&&(!r2)&&(!r3)&&(!r4)&&(!sameCardType)&&(!straightIf)){
			cardLevel=1;
		    System.out.println(output[a[4]-2]+"-high");
		    }
	}
	
	/*The selectionsort, indexOfSmallest and interchange methods are copied 
	 * from 'Java: An Introduction to Problem Solving and Programming', fourth
	 *  edition, from page 133 to 134, the author is Walter Savitch.*/
	private static void selectionsort(int[] a){
		int index,indexOfNextSmallest;
		for (index=0;index<(a.length-1);index++){
			indexOfNextSmallest = indexOfSmallest(index, a);
			interchange(index,indexOfNextSmallest,a);
		}
	}
	
    private static int indexOfSmallest(int startIndex, int[] a){
    	int min = a[startIndex];
    	int indexOfMin = startIndex;
    	int index;
    	for(index=(startIndex+1); index<a.length; index++){
    		if(a[index]<min){
    			min = a[index];
    			indexOfMin = index;
    		}
    	}
    	return indexOfMin;
    }
	
    private static void interchange(int i,int j,int[] a){
        int temp;
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    
    /*The cardNumber method changes the first char in the string to integer 
     * and determine that if the integer is valid or not.*/
	private int cardNumber(String card){
		char cardNum;
		int cardValue;
		String cardLowerCase = card.toLowerCase();
		cardNum = cardLowerCase.charAt(0);
        
		switch(cardNum){
		case't': cardValue=10; break;
		case'j': cardValue=11; break;
		case'q': cardValue=12; break;
		case'k': cardValue=13; break;
		case'a': cardValue=14; break;
		default: 
			String s=String.valueOf(cardNum);
			cardValue=Integer.parseInt(s);
			break;
		}

		if(cardValue>14 || cardValue<2){
			System.out.printf("Error: invalid card name '%s'%n",card);
			System.exit(0);
		}
		
	return cardValue;	
	} 
   
	/*The cardType method extract the second char in the string and determine
	 * that if the integer is valid or not.*/
	private char cardType(String card){
		char cardName;
		String cardLowerCase = card.toLowerCase();
		cardName = cardLowerCase.charAt(1);
        
		switch(cardName){
		case'c': 
		case'd': 
		case'h': 
		case's': 
            break;
		default: 
			System.out.printf("Error: invalid card name '%s'%n",card);
			System.exit(0);
			break;
		}
	return cardName;	
	} 
	
	/*The method setRank provide an array that sort the five card value by the 
	 * importance. For example, if the five cards are "2,3,3,3,9", the rank 
	 * array will sort them to {3,3,3,9,2}*/
	public void setRank(){
		relation();
		//Firstly, sort the five card values from highest to lowest. 
		rank[0]=cardvalue[4];
		rank[1]=cardvalue[3];
		rank[2]=cardvalue[2];
		rank[3]=cardvalue[1];
		rank[4]=cardvalue[0];
		
		//Select the special situations that rank array need to fix.
		if(r1&&r2&&r3&&(!r4)){
			rank[0]= cardvalue [0];
			rank[4]= cardvalue [4];
            }

        if(r1&&r2&&(!r3)&&r4){
	        rank[0]= cardvalue [0];
			rank[1]= cardvalue [1];
			rank[3]= cardvalue [3];
			rank[4]= cardvalue [4];
            }

        if(r1&&r2&&(!r3)&&(!r4)){
			rank[0]= cardvalue [0];
			rank[1]= cardvalue [1];
			rank[3]= cardvalue [4];
			rank[4]= cardvalue [3];
            }

        if((!r1)&&r2&&r3&&(!r4)){
			rank[0]= cardvalue [3];
			rank[1]= cardvalue [1];
			rank[3]= cardvalue [4];
			rank[4]= cardvalue [0];
            }

        if(r1&&(!r2)&&(!r3)&&r4){
			rank[2]= cardvalue [0];
			rank[4]= cardvalue [2];
            }

        if(r1&&(!r2)&&r3&&(!r4)){
			rank[0]= cardvalue [3];
			rank[1]= cardvalue [2];
			rank[2]= cardvalue [1];
			rank[3]= cardvalue [0];
			rank[4]= cardvalue [4];
            }

		if(r1&&(!r2)&&(!r3)&&(!r4)){
			rank[0]= cardvalue [0];
			rank[1]= cardvalue [1];
			rank[2]= cardvalue [4];
			rank[3]= cardvalue [3];
			rank[4]= cardvalue [2];
            }

        if((!r1)&&r2&&(!r3)&&(!r4)){
			rank[0]= cardvalue [2];
			rank[1]= cardvalue [1];
			rank[2]= cardvalue [4];
			rank[3]= cardvalue [3];
            }

        if((!r1)&&(!r2)&&r3&&(!r4)){
			rank[0]= cardvalue [2];
			rank[2]= cardvalue [4];
            }
	}
		
}