/*This project was written for the project of the subject "Programming and 
 * Software Development" (COMP90041).The name of this project is "The Game 
 * of Poker", which can realize simple five-card game.*/

/*Poker class is the main class in this project. This class can calculate the
 * number of players and compare each player's cards to determine winners.*/

public class Poker{
	public static void main(String[] args){
	/*calculate the number of players and if command line arguments do not meet
	 * requirements, then output prompt and exit program.*/
	int playerNum=0;
	int cardNum=5;
	if((args.length)%cardNum==0 && (args.length)!=0 ){ 
        playerNum=(args.length)/cardNum;   
	}else{
		System.out.println("Error: wrong number of arguments; "
				+ "must be a multiple of 5");
		System.exit(0);	
	}
    
    /*Define array 'player' to store every player's different data. From line26 
     * to line50, these codes can characterize every player's poker hand 
     * correctly by using methods in PockerHand.class.*/
    String card1, card2, card3, card4, card5;
    PokerHand[] player = new PokerHand[playerNum];
    int[] playerLevel = new int[playerNum];
    int i;

	for(i=0; i<playerNum; i++){
		
		card1 = args[0+cardNum*i];
		card2 = args[1+cardNum*i];
		card3 = args[2+cardNum*i];
		card4 = args[3+cardNum*i];
        card5 = args[4+cardNum*i];
        
        player[i] = new PokerHand();
        player[i].valueName(card1,card2,card3,card4,card5);
        System.out.print("Player "+(i+1)+": ");
        player[i].sortArray();
        player[i].pokerClass();
        /*Array playerLevel is defined to store players' types of poker hand 
         * in turn. Players' types of poker hand are described by the integer 
         * cardLevel in PokerHand class.This array will use to compare which 
         * player can win in the game.*/ 
        playerLevel[i] = player[i].getCardLevel();
        //Sort the player's card according to comparing order.
        player[i].setRank();
	}
	
    if(playerNum==1){
    	System.exit(0);
    }
    
	/*This part is to compare each player's cards and find the winner. If some
	 * players draw, then output all of them.*/
	int winner,winnerNum;
	winner=0;//Assume that player 1 is the winner in the beginning.
	winnerNum=1;
	    
	int j,k,n,rank1,rank2;
	n=0;
	//This array is to store all winners player's number in ascending order.
	int[] winnerArray=new int[playerNum];
	winnerArray[0]=1;
	for(j=1;j<playerNum;++j){
	    //if player(j+1) wins, than change the winner to (j+1)
	 	if(playerLevel[j]>playerLevel[winner]){
	        winner = j;
	        winnerNum=1;
	        //Reset winnerArray, delete all winners before player(j+1).
	        setArray(winnerArray);
	        winnerArray[0]=(j+1); 
	    //Compare if the kinds of poker hand are same between two players.
	    }else if(playerLevel[j]==playerLevel[winner]){
	    /*Compare the value of two player's card according to array 
	     * rank in turn in the PokerHand.class. 
	     * The array rank provides the correct ordering of cards to 
	     * consider which poker hand can win.*/
	         for(k=0; k<5; k++){
	    	     rank1=player[j].getRank()[k];
	    		 rank2=player[winner].getRank()[k];
	  
	    		 if(rank1>rank2){
	    	         winner = j;
	    	         winnerNum=1;
	    	         setArray(winnerArray);
	    	         winnerArray[0]=(j+1);
	    	         break;
	    	     }else if(rank1<rank2){
	    	         break;
	    	     }else if(rank1==rank2){
	    	         ++n;
	    		}
	    	}
	    		
	        //If all five cards are same, the two players draw.
	    	if(n==5){
	    	    winnerArray[winnerNum]=(j+1);
	    		winnerNum=winnerNum+1;
	    	}
	    	n=0;
	    }
	}
	    
	//Output the numbers in the winnerArray in turn.
    int m,playerNo;
    if (winnerNum==1){
    System.out.println("Player "+ (winnerArray[0]) +" wins.");
    }else{
        System.out.print("Players ");
        for(m=0;m<winnerNum-2;m++){
            playerNo=winnerArray[m];
        	System.out.print(playerNo+", ");
        }
        System.out.println((winnerArray[winnerNum-2])+" and "
                +(winnerArray[winnerNum-1])+" draw.");
    }
	    
    }
	
	//This method is used to delete all numbers in the winnerArray.
	//In this array '-1' means empty.
	public static int[] setArray(int[] a){
		int i; 
		for(i=0; i<a.length;i++){
			a[i]=-1;
		}		
		return a;
		}
}


