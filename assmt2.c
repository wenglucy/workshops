/*
Author: Weng Luxin
Student ID: 707841
Date: 20 Oct 2015
Description: This project is written for Engineering Computation (COMP20005)
assignment 2. The topic of this project is noise polltion. The program can model
the noisepatterns across a given region, and calculate any zones that perceive 
an excessive level of noise.
*/
#include <stdio.h>
#include <math.h>

#define MAX_SOURCES 99
#define MAX_POINTS 99
#define MAX_VERTICES 99
#define LOG_CONSTANT log(10.0)
/*The total number of the points in a 7800*7800 square.*/
#define REGION_POINTS 5929 
#define REGION_SIDE_POINTS 78
#define GRID_LENGTH 100
#define NOISELIMIT 85
#define REGION_CELLS 3042
#define CELL_WIDTH 100
#define CELL_HEIGHT 200
#define CELL_TOP 7700
#define CELL_BOTTOM 100
#define CELL_LEFT 50
#define CELL_RIGHT 7750

/*----------------------------------------------------------------------------*/
typedef struct {
	double x;
	double y;
} point_t;

typedef struct {
	point_t location;
	double decibel;
} noise_source_t;

typedef struct {
	point_t position;
	double gridLevel;
} grid_t;

typedef struct {
	point_t centre;
	double cellLevel;
} cell_t;

typedef struct {
	point_t p1;
	point_t p2;
} line_t;

/*----------------------------------------------------------------------------*/
double distance(point_t p1, point_t p2);

double noise_level(double noiseLevelOne, double distance);

double L_sum(noise_source_t noises[], int noise_Num, point_t point);

void output_stage4(double cellLevel);

int lineIntersect(line_t l1, line_t l2);

/*----------------------------------------------------------------------------*/
int 
main(int argc, char* argv[]){
	noise_source_t noises[MAX_SOURCES];
	point_t points[MAX_POINTS];
	point_t polygon[MAX_VERTICES];
	int noise_Num=0;
	/*In the array points, the points[0] is (0,0), so point_Num starts from 1.*/
	int point_Num=1;
	int polygon_Num=0;

	double a, b, c;/*Temporary Variable*/
	char next;
	
	while(scanf("%c",&next)!=EOF){
		if(next=='N'){
			/*Store all noice sources in the array noises.*/
            scanf("%lf%lf%lf",&a,&b,&c);
            noises[noise_Num].location.x = a;
            noises[noise_Num].location.y = b;
            noises[noise_Num].decibel = c;   
		    noise_Num=noise_Num+1;
		
		}else if(next=='P'){
			/*Store all observation points in the array points.*/
		    scanf("%lf%lf",&a,&b);
            points[point_Num].x = a;
            points[point_Num].y = b;
            point_Num=point_Num+1;
		}
		
		else if(next=='V'){
			/*Store all vertexes of the polygon in the array polygon.*/
		    scanf("%lf%lf",&a,&b);
            polygon[polygon_Num].x = a;
            polygon[polygon_Num].y = b;
            polygon_Num=polygon_Num+1;
	    }
	}
	
/*----------------------------------stage1------------------------------------*/
	points[0].x = 0.0;
	points[0].y = 0.0;
	double LSum;
	/*calculate the aggregate noise level at the origin (0,0).*/
    LSum = L_sum(noises, noise_Num, points[0]);
    printf("Stage 1\n==========\n");
    printf("Number of noise sources: %02d\n", noise_Num);
    printf("Noise level at (0000.0, 0000.0): %05.2f dB\n", LSum);
	
/*----------------------------------stage2------------------------------------*/
    printf("\nStage 2\n==========\n");
	int i;
	for(i=1;i<point_Num;i++){
	/*calculate the aggregate noise level at the points[i].*/
	LSum = L_sum(noises, noise_Num, points[i]);
	printf("Noise level at (%06.1f, %06.1f): %05.2f dB\n", 
		points[i].x, points[i].y, LSum);
	}
	
/*----------------------------------stage 3-----------------------------------*/
	int j;
	int k;
	int m;
	/*counts the percentage of the points whose noise level is larger than or 
	equal to the 85 dB*/
	int hugeNoise;
	hugeNoise=0;
	m=0;
	grid_t grids[REGION_POINTS];
	for(j=1;j<REGION_SIDE_POINTS;j++){
	    for(k=1;k<REGION_SIDE_POINTS;k++){
	    grids[m].position.x=j*GRID_LENGTH;
	    grids[m].position.y=k*GRID_LENGTH;
	    /*calculate the aggregate noise level at the grids[m].*/
	    grids[m].gridLevel= L_sum(noises, noise_Num, grids[m].position);
	    if(grids[m].gridLevel>=NOISELIMIT){
	    hugeNoise=hugeNoise+1;
	    }
	    m=m+1;
	    }
	}
	double  hugeNoise_Percent;
	/*Calculate the percentage of the huge noise in the total points.*/
	hugeNoise_Percent=(hugeNoise*1.0/REGION_POINTS)*100;
	printf("\nStage 3\n==========\n");
	printf("5929 points sampled\n");
	printf("%04d points (%05.2f%) have noise level >= 85 dB\n", 
		hugeNoise, hugeNoise_Percent);
	 
/*----------------------------------stage4------------------------------------*/
    printf("\nStage 4\n==========\n");
	int p;
	int q;
	int r=0;
	/*Store the datas of every cell in the array cells.*/
	cell_t cells[REGION_CELLS];
	/*Print out the character represents each cell from top to bottom,from left
	to right.*/
	for(p=CELL_TOP; p>=CELL_BOTTOM; p=p-CELL_HEIGHT){
	    for(q=CELL_LEFT; q<=CELL_RIGHT; q=q+CELL_WIDTH){
	    	cells[r].centre.x=q;
	        cells[r].centre.y=p;
	        cells[r].cellLevel= L_sum(noises, noise_Num, cells[r].centre);
	        output_stage4(cells[r].cellLevel);
	        r=r+1;
	    }
	    printf("\n");
	}
	
/*----------------------------------stage5------------------------------------*/
	printf("\nStage 5\n==========\n");
	/*Build an array that store all edges of the polygon.*/
	line_t edges[MAX_VERTICES];
	int h;
	for(h=0;h<polygon_Num-1;h++){
	    edges[h].p1=polygon[h];
	    edges[h].p2=polygon[h+1];
	}
	edges[polygon_Num-1].p1=polygon[polygon_Num-1];
	edges[polygon_Num-1].p2=polygon[0];
	
	int s;
	int t;
	int z;
	int w=0;
	int value;
	line_t segment;
	r=0;
	for(s=CELL_TOP; s>=CELL_BOTTOM; s=s-CELL_HEIGHT){
	    for(t=CELL_LEFT; t<=CELL_RIGHT; t=t+CELL_WIDTH){
	    /*Create a segment from (0,s) to (t,s) */
	    segment.p1.x=0;
	    segment.p1.y=s;
	    segment.p2.x=t;
	    segment.p2.y=s;
	        /*Test if this segment touch any edges in the polygon.*/
	        for(z=0;z<polygon_Num;z++){
	        	value=lineIntersect(segment, edges[z]);
	        	w=w+value;
	        }
	        
	        /*If the segment only touch one edge, that means the point is in the
	        polygon. If the segment does not touch any edges or touch more than 
	        one edge, the point is out of the polygon.*/
	        if(w==1){
	            output_stage4(cells[r].cellLevel);
	        }else{
	            printf("#");
	        }
	        w=0;
	        r=r+1;
	    }
	    printf("\n");
	}
return 0;
}

/*Print out related character with the noise level.
The relationship graph:
 <20  <30  <40.0  <50  <60  <70  <80  <85  ��85
��-���� �� ��4�� �� ����6���� ����8���� ����+��*/
void
output_stage4(double cellLevel){
if(cellLevel<20){
    printf("_");
}else if(cellLevel<30){
    printf(" ");
}else if(cellLevel<40.0){
    printf("4");
}else if(cellLevel<50){
    printf(" ");
}else if(cellLevel<60){
    printf("6");
}else if(cellLevel<70){
    printf(" ");
}else if(cellLevel<80){
    printf("8");
}else if(cellLevel<85){
    printf(" ");
}else{
    printf("+");
}
}

/*Calculate the aggregate noise level.*/
double 
L_sum(noise_source_t noises[], int noise_Num, point_t point){
    int n;
	double sum;
	double levelTwo;
	double dis;
	sum=0.0;
	/*Use the noise source array to calculate the noise level of each noise 
	source.*/
	for(n=0;n<noise_Num;n++){
	dis = distance(noises[n].location, point);
	levelTwo = noise_level(noises[n].decibel, dis);
	sum = sum + pow(10,(levelTwo/10));
	}
	double LSum;
	LSum=10.0*(log(sum)/LOG_CONSTANT);
	
	if(LSum<0){
	LSum=0.0;
	}
	return LSum;
}

/*Calculate the noise L2 by using noise L1 and the distance between them.*/
double
noise_level(double noiseLevelOne, double distance){
	double noiseLevelTwo;
    noiseLevelTwo = noiseLevelOne + 20*(log(1/distance)/LOG_CONSTANT);
    if(noiseLevelTwo<0){
    noiseLevelTwo=0.0;
    }
    return noiseLevelTwo;
}

/*Calculate the distance between any two points.*/
double
distance(point_t p1, point_t p2) {
	double result = 0.0;
	double deltax, deltay;
	deltax = p1.x - p2.x;
	deltay = p1.y - p2.y;
	result = sqrt(deltax*deltax + deltay*deltay);
	return result;
}

/* =============================================================== */

/* This function was adapted in 2012 from 
 * http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/pdb.c
 * (no longer available at that URL in 2013)
 * and was written in the first instance by Paul Bourke
 *
 * Modified for use in a different assignment previously by Alistair Moffat by:
 *   . changing the argument type to two structs type line_t
 *   . making sure result is TRUE if an endpoint is on the other line
 * Modified for use by Jianzhong Qi by:
 *   . testing whether the projections of the two line segments intersect first
 */
#define TRUE 1
#define FALSE 0
#define EPS (1e-06)
#define ABS(x) (fabs(x))
#define MIN(a,b) (a<b ? a:b)
#define MAX(a,b) (a>b ? a:b)

int lineIntersect(line_t l1, line_t l2) {
   double x1=l1.p1.x, y1=l1.p1.y,
   	  x2=l1.p2.x, y2=l1.p2.y,
   	  x3=l2.p1.x, y3=l2.p1.y,
   	  x4=l2.p2.x, y4=l2.p2.y;
   double mua,mub;
   double denom,numera,numerb;

   /* Take the projections of the two line segments */
   double xMin1, xMax1, xMin2, xMax2, yMin1, yMax1, yMin2, yMax2;
   xMin1 = MIN(x1, x2);
   xMax1 = MAX(x1, x2);
   xMin2 = MIN(x3, x4);
   xMax2 = MAX(x3, x4);

   yMin1 = MIN(y1, y2);
   yMax1 = MAX(y1, y2);
   yMin2 = MIN(y3, y4);
   yMax2 = MAX(y3, y4);
   
   /* Do the projects intersect? */
   if((xMin2-xMax1) >= EPS || (xMin1-xMax2) >= EPS || 
   	   (yMin2-yMax1) >= EPS || (yMin1-yMax2) >= EPS) {
   	   return FALSE;
   }

   denom  = (y4-y3) * (x2-x1) - (x4-x3) * (y2-y1);
   numera = (x4-x3) * (y1-y3) - (y4-y3) * (x1-x3);
   numerb = (x2-x1) * (y1-y3) - (y2-y1) * (x1-x3);

   /* Are the line coincident? */
   if (ABS(numera) < EPS && ABS(numerb) < EPS && ABS(denom) < EPS) {
      return(TRUE);
   }

   /* Are the line parallel */
   if (ABS(denom) < EPS) {
      return(FALSE);
   }

   /* Is the intersection along the the segments */
   mua = numera / denom;
   mub = numerb / denom;
   /* AM - use equality here so that "on the end" is not an
    * intersection; use strict inequality if "touching at end" is an
    * intersection */
   if (mua < 0 || mua > 1 || mub < 0 || mub > 1) {
      return(FALSE);
   }
   return(TRUE);
}
