/**
 * The sensor to identify all the bicycles at the segment 3
 */
public class Sensor extends BicycleHandlingThread {

    protected Belt belt;
    protected Robot robot;
    //the bicycle that the sensor is identifying
	Bicycle identifiedBicycle = null;
	
    /**
     * Create a new Sensor to identify all the bicycles at the segment 3.
     */
    Sensor(Belt belt, Robot robot) {
        super();
        this.belt = belt;
        this.robot = robot;
    }

    /**
     * Check all the bicycles at the segment 3, identify the bicycles with tags
     * and notify the robot to remove the bicycle on the segment 3 immediately.
     */
    public void run(){

        while (!isInterrupted()) {
        	try {
				checkBicycle();
			} catch (InterruptedException e) {
				this.interrupt();
			}
        }
        System.out.println("Sensor terminated");
    }
    
    /**
     * Check the bicycles at the segment 3
     *
     * @throws InterruptedException
     *            if the thread executing is interrupted.
     */
    public synchronized void checkBicycle() throws InterruptedException {
    	//find the bicycle which will be checked
        identifiedBicycle = belt.peek(Params.SEGMENT_THREE);
        
        if(identifiedBicycle!=null && (!identifiedBicycle.isIdentified())){
        	
        	if(identifiedBicycle.isTagged()){
        		System.out.println("S: ("+identifiedBicycle+") is tagged");
        		// mark the bicycle after it has been identified
        		identifiedBicycle.setIdentified(); 
        		
        		//when sensor identifies a new tagged bicycle and the robot or
        		//the inspector still do not finish the old one, the sensor ask
        		//belt to stop moving.
        		if(belt.getRobotOccupied()){
        			belt.sensorNotifyBelt();
        		}
        		
        		//the sensor ask the robot to remove the tagged bicycle.
        		robot.setSensorCallRobot();
                robot.notifyRobot();
        	}
        }
    }
    
}
