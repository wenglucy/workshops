/**
 * The bicycle quality control belt
 */
public class Belt {
	// the name of the belt
    protected String beltName;
    // the items in the belt segments
    protected Bicycle[] segment;

    // the length of this belt
    protected int beltLength;

    // to help format output trace
    final private static String indentation = "                  ";

	private boolean robotOccupied = false;
	
	protected boolean moverStop = false;

    /**
     * Create a new, empty belt, initialised as empty
     */
    public Belt(int beltLength, String beltName){
    	this.beltLength = beltLength;
    	this.beltName = beltName;
        segment = new Bicycle[beltLength];
        for (int i = 0; i < segment.length; i++) {
            segment[i] = null;
        }
    }
    
    /**
     * Put a bicycle on the belt.
     * 
     * @param bicycle
     *            the bicycle to put onto the belt.
     * @param index
     *            the place to put the bicycle
     * @throws InterruptedException
     *            if the thread executing is interrupted.
     */
    public synchronized void put(Bicycle bicycle, int index)
            throws InterruptedException {

    	// while there is another bicycle in the way, block this thread
        while (segment[index] != null) {
            wait();
        }

        // insert the element at the specified location
        segment[index] = bicycle;

        // make a note of the event in output trace
        System.out.println(beltName + ": "+ bicycle + " arrived");

        // notify any waiting threads that the belt has changed
        notifyAll();
    }
    
    public synchronized Bicycle remove(int index)
    		throws InterruptedException {

    	// while the segment has no bicycle, block this thread
        while (segment[index] == null) {
            wait();
        }

        // remove the element at the specified location
        Bicycle bicycle = segment[index];
        
        segment[index] = null;
        // make a note of the event in output trace
        System.out.println(bicycle + " removed from the long belt.");

        // notify any waiting threads that the belt has changed
        notifyAll();
        
        return bicycle;
    }
    
    /**
     * Take a bicycle off the end of the belt
     * 
     * @return the removed bicycle
     * @throws InterruptedException
     *             if the thread executing is interrupted
     */
    public synchronized Bicycle getEndBelt() throws InterruptedException {

        Bicycle bicycle;

        // while there is no bicycle at the end of the belt, block this thread
        while (segment[segment.length-1] == null) {
            wait();
        }

        // get the next item
        bicycle = segment[segment.length-1];
        segment[segment.length-1] = null;

        // make a note of the event in output trace
        System.out.print(indentation + indentation);
        System.out.println(bicycle + " departed");

        // notify any waiting threads that the belt has changed
        notifyAll();
        return bicycle;
    }

    /**
     * Move the belt along one segment
     * 
     * @throws OverloadException
     *             if there is a bicycle at position beltLength.
     * @throws InterruptedException
     *             if the thread executing is interrupted.
     */
    public synchronized void move() 
            throws InterruptedException, OverloadException {
        // if there is something at the end of the belt, 
    	// or the belt is empty, do not move the belt
    	// or the robot cannot remove the tagged bicycle immediately
        while (isEmpty() || segment[segment.length-1] != null || moverStop) {
            wait();
        }

        // double check that a bicycle cannot fall of the end
        if (segment[segment.length-1] != null) {
            String message = "Bicycle fell off end of " + " belt";
            throw new OverloadException(message);
        }

        // move the elements along, making position 0 null
        for (int i = segment.length-1; i > 0; i--) {
            if (this.segment[i-1] != null) {
                System.out.println(
                		indentation + beltName + ": "+
                		this.segment[i-1] +
                        " [ s" + (i) + " -> s" + (i+1) +" ]");
            }
            segment[i] = segment[i-1];
        }
        segment[0] = null;
//        System.out.println(indentation + this);
        
        // notify any waiting threads that the belt has changed
        notifyAll();
    }

    /**
     * @return the maximum size of this belt
     */
    public int length() {
        return beltLength;
    }

    /**
     * Peek at what is at a specified segment
     * 
     * @param index
     *            the index at which to peek
     * @return the bicycle in the segment (or null if the segment is empty)
     */
    public Bicycle peek(int index) {
        Bicycle result = null;
        if (index >= 0 && index < beltLength) {
            result = segment[index];
        }
        return result;
    }

    /**
     * Check whether the belt is currently empty
     * @return true if the belt is currently empty, otherwise false
     */
    private boolean isEmpty() {
        for (int i = 0; i < segment.length; i++) {
            if (segment[i] != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return java.util.Arrays.toString(segment);
    }

    /*
     * @return the final position on the belt
     */
    public int getEndPos() {
        return beltLength-1;
    }
    
    // tell sensor the robot is working
    public void robotOccupied(){
    	robotOccupied = true;
    }
    
    // tell sensor the robot can delivery a new bicycle
    public synchronized void notRobotOccupied(){
    	moverStop = false;
    	robotOccupied = false;
    	notifyAll();
    }
    
    public boolean getRobotOccupied(){
    	return robotOccupied ;
    }
    
    // when sensor identifies a tagged car and the robot is still working,
    // block the thread
    public boolean sensorNotifyBelt(){
    	moverStop = true;
    	System.out.println("Stop moving the long belt");
    	return moverStop;
    }
    
    public String getBeltName() {
        return this.beltName;
    }
}
