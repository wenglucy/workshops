/**
 * The robot to remove or send back the bicycle to belt
 */
public class Robot extends BicycleHandlingThread {

    protected Belt longBelt;
    protected Belt shortBelt;
    // the bicycle that the robot is delivering 
    protected Bicycle bicycle = null;
    //protected boolean robotOccupied = false;
    protected Inspector inspector;
    protected boolean sensorCallRobot = false;

    /**
     * Create a new robot
     */
    Robot(Belt longBelt, Belt shortBelt) {
        super();
        this.longBelt = longBelt;
        this.shortBelt =shortBelt;
    }

    /**
     * Call the inspector that the robot will send the bicycle for
     */
    public synchronized void callInspector(Inspector inspector){
    	this.inspector = inspector;
    }

    /**
     * The thread's main method. 
     * Send the bicycle to the inspector or the belt according to the different
     * conditions.
     */
    public void run() {
        while (!isInterrupted()) {
        	try {
        		if(inspector.getRobotCalledByInspector() == true){
        			sendBicycleToBelt();
        			longBelt.notRobotOccupied();

        		}else if (robotCalledBySensor() == true){
        			longBelt.robotOccupied();
				    sendBicycleToInspector();
				    
				}else{
					// if both sensor and inspector do not call the robot, the 
					// robot should wait the notify.
					waitRobot();
				}
        		
			} catch (InterruptedException e) {
				this.interrupt();
			}
        }
        System.out.println("Robot terminated");
    }
    
    // if the robot has delivered the bicycle to the belt, make the robot 
    // do not record any information about that bicycle.
    public void cleanRobot(){
    	bicycle = null;
    }
    
    /**
     * @return the bicycle that the robot is controlling
     */
    public synchronized Bicycle getRobotBicycle(){
    	return bicycle;
    }
    
    /**
     * Send the bicycle to the inspector
     * @throws InterruptedException
     *              if the thread executing is interrupted
     */
    public synchronized void sendBicycleToInspector() 
    		throws InterruptedException{
    	
        while (robotCalledBySensor() != true) {
            wait();
        }
            
    		bicycle = longBelt.remove(2);
    		System.out.println("R: the robot starts to send ("+ this.bicycle +") to inspect");
    		setSensorNotCallRobot();
    		int sleepTime = Params.ROBOT_MOVE_TIME;
            sleep(sleepTime);
            
            // the robot asks the inspector to start inspecting
            inspector.setInspectorCalledByRobot();
            inspector.notifyInspector();
    }
    
    /**
     * Send the bicycle to the short belt
     * @throws InterruptedException
     *              if the thread executing is interrupted
     */  
    public synchronized void sendBicycleToBelt() throws InterruptedException{
    	
        while (inspector.getRobotCalledByInspector() != true) {
            wait();
        }
        
        System.out.println("R: the robot starts to send ("+ this.bicycle +") "
        		+ "to the short belt");
        if(this.bicycle != null){
        	shortBelt.put(this.bicycle, 0);	
        	System.out.println(this.bicycle+" has put to the short belt");
        	cleanRobot();
        }else{
        	System.out.println("the bicycle has lost");
        }
    	
        inspector.setNotRobotCalledByInspector();
    }
   
    /**
     * Notify the robot to restart to work
     * @throws InterruptedException
     *              if the thread executing is interrupted
     */ 
    public synchronized void notifyRobot() throws InterruptedException {
            notifyAll();
    }
    
    /**
     * Ask the robot to stop work and wait others to notify it
     * @throws InterruptedException
     *              if the thread executing is interrupted
     */ 
    
    public synchronized void waitRobot() throws InterruptedException {
        wait();
    }
    
    /**
     * Sensor will call this method to tell the robot start work
     */ 
    public void setSensorCallRobot() {
        sensorCallRobot = true;
    }
    
    /**
     * Robot finishes the work that the sensor gives it 
     */ 
    public void setSensorNotCallRobot() {
        sensorCallRobot = false;
    }
    
    /**
     * find if the robot is work for the sensor
     * @return sensor call robot or not
     */ 
    public boolean robotCalledBySensor() {
        return sensorCallRobot;
    }
    
}
