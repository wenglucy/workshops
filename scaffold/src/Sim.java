/**
 * The driver of the simulation 
 */

public class Sim {
    /**
     * Create all components and start all of the threads.
     */
    public static void main(String[] args) {
        //create two belt with different length
    	Belt longBelt = new Belt(Params.LONG_BELT_LENGTH, "LongBelt");
        Belt shortBelt = new Belt(Params.SHORT_BELT_LENGTH, "ShortBelt");
        Producer producer = new Producer(longBelt);
        //create two consumers to accept the bicycles from both belt
        Consumer consumer1 = new Consumer(longBelt);
        Consumer consumer2 = new Consumer(shortBelt);
        
        BeltMover mover1 = new BeltMover(longBelt);
        BeltMover mover2 = new BeltMover(shortBelt);
        
        Robot robot = new Robot(longBelt, shortBelt);
        Inspector inspector = new Inspector(robot);
        Sensor sensor = new Sensor(longBelt,robot);
        
        robot.callInspector(inspector);
        
        consumer1.start();
        consumer2.start();
        
        producer.start();
        mover1.start();
        mover2.start();
        
        sensor.start();
        robot.start();
        inspector.start();

        while (consumer1.isAlive() && 
        	   consumer2.isAlive() &&
               producer.isAlive() && 
               mover1.isAlive() && 
               mover2.isAlive() && 
               sensor.isAlive() &&
               robot.isAlive()&&
               inspector.isAlive())
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                BicycleHandlingThread.terminate(e);
            }

        // interrupt other threads
        consumer1.interrupt();
        consumer2.interrupt();
        
        producer.interrupt();
        
        mover1.interrupt();
        mover2.interrupt();
        
        sensor.interrupt();
        robot.interrupt();
        inspector.interrupt();

        System.out.println("Sim terminating");
        System.out.println(BicycleHandlingThread.getTerminateException());
        System.exit(0);
    }
}
