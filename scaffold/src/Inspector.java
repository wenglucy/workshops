/**
 * A inspector to inspect the bicycle good or not, and remove the tag for
 * the good bicycles
 */
public class Inspector extends BicycleHandlingThread {
    //the robot that send the bicycle to the inspector
    protected Robot robot;
    //if the robot call the inspector to check the bicycle, it is true
    protected boolean inspectorCalledByRobot = false;
    //if the inspector call the robot to send the bicycle, it is true
    protected boolean robotCalledByInspector = false;

    /**
     * Create a new Inspector to check the bicycle
     */
    Inspector(Robot robot) {
        super();
        this.robot = robot;
    }

    /**
     * The thread's main method. 
     * Inspect the bicycle that the robot sends to it
     */
    public void run() {
    	 while (!isInterrupted()) {
    		
        	try {
				inspectBicycle();
			} catch (InterruptedException e) {
				this.interrupt();
			}
        }
        System.out.println("Inspector terminated");
    }
    
    /**
     * The inspector is inspect the bicycle that the robot send to it 
     * @throws InterruptedException
     *             if the thread executing is interrupted
     */
    public synchronized void inspectBicycle()
            throws InterruptedException {
        
    	// when no bicycle needs to be inspected, wait
        while (inspectorCalledByRobot != true) {
            wait();
        }
    
        System.out.println
            ("I: start to inspect (" + robot.getRobotBicycle()+").");
        int sleepTime = Params.INSPECT_TIME;
        sleep(sleepTime);
        
        // decide if the bicycle is non-detective, remove the tag
    	if(robot.getRobotBicycle().isDefective()){
    		robot.getRobotBicycle().setTagged();
    		System.out.println("I: sorry, the bicycle (" + 
    		robot.getRobotBicycle()+") is detective");
    	}else{
    		robot.getRobotBicycle().setNotTagged();
    		System.out.println("I: congratulations, the bicycle (" + 
    	    		robot.getRobotBicycle()+") is non-detective");
    	}
    	
    	setNotInspectorCalledByRobot();
    	// ask the robot to send the bicycle back
        setRobotCalledByInspector();
        robot.notifyRobot();
        notifyAll();
    }
    
   
   // notify the inspector when the robot send a bicycle to it 
    public synchronized void notifyInspector() throws InterruptedException {
        if(inspectorCalledByRobot == true) {
            notifyAll();
        }
    }
    
    // tell the inspector that the robot send a bicycle to it
    public synchronized void setInspectorCalledByRobot(){
    	inspectorCalledByRobot = true;
    }
    
    // the inspector does not have a bicycle
    public synchronized void setNotInspectorCalledByRobot(){
    	inspectorCalledByRobot = false;
    }
    
    // @return the inspector has a bicycle or not
    public synchronized boolean getInspectorCalledByRobot(){
    	return inspectorCalledByRobot;
    }
    
    // tell the robot that the inspector has finished one bicycle
    public synchronized void setRobotCalledByInspector(){
    	robotCalledByInspector = true;
    }
    
    // the robot finish send the bicycle to the belt
    public synchronized void setNotRobotCalledByInspector(){
    	robotCalledByInspector = false;
    }
    
    // @return the robot has send the bicycle to the belt or not
    public synchronized boolean getRobotCalledByInspector(){
    	return robotCalledByInspector;
    }
}
